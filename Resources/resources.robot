*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${BROWSER}                  chrome
${URL_QA}                   https://qa.decormartph.com
${Login_Button}             xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button[1]
${Submit_Login_Button}      xpath://*[@id="app"]/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/div[2]/button
${Mobile_User}              9950129583
${Mobile_Pass}              Password1
${Email_User}               dmqa14@yopmail.com
${Email_Pass}               Password1
${FB_User}                  kanchala.test@gmail.com
${FB_Pass}                  kanchalatest26111
${Avatar}                   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button/span
${Input_Search}             xpath://*[@id="input-25"]

*** Keywords ***
Open Browser And Maximize
    Open Browser                    ${URL_QA}  ${BROWSER}
    Maximize Browser Window
    Wait Until Element Contains     xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button[1]/span      SIGN IN
    Sleep  2s

Close Browser Window
    Close Browser
    Log To Console  Test Completed

Login To mobile account
    Click button                    ${Login_Button}
    Input Text                      xpath://*[@id="input-476"]  ${Mobile_User}
    Input PASSWORD                  xpath://*[@id="input-480"]  ${Mobile_Pass}
    Click button                    ${Submit_Login_Button}
    Sleep                           2s

Login To Email account
    Click button                    ${Login_Button}
    Click element                   xpath://*[@id="app"]/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/div[2]/div[1]/div/div[2]/div/a[2]
    Wait Until Element Contains     xpath://*[@id="email"]/div/div/div/div[1]/span      Email
    Input Text                      xpath://*[@id="input-492"]  ${Email_User}
    Input PASSWORD                  xpath://*[@id="input-495"]  ${Email_Pass}
    Click button                    ${Submit_Login_Button}
    Sleep                           2s

Login To Facebook account
    Click button                    ${Login_Button}
    Click button                    xpath://*[@id="app"]/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/div[3]/div/div[2]/div[2]/button
    Input Text                      xpath://*[@id="email"]  ${FB_User}
    Input PASSWORD                  xpath://*[@id="pass"]   ${FB_Pass}
    Click button                    xpath://*[@id="loginbutton"]
    Sleep                           3s