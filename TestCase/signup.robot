*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${BROWSER}      chrome
${URL}          https://qa.decormartph.com
${TempOTP}      https://www.otp4free.com/ph639950129743/

*** Keywords ***


*** Test Cases ***
Sign up by mobile
    Open BROWSER                    ${URL}  ${BROWSER}
    Maximize Browser Window
    Wait Until Element Contains     xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button[2]       REGISTER
    Click button                    xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button[2]
    Input Text                      xpath://*[@id="input-509"]                                                                          09950129743
    Input PASSWORD                  xpath://*[@id="input-513"]                                                                          mew123456
    Input Text                      xpath://*[@id="input-517"]                                                                          kanchala
    Input Text                      xpath://*[@id="input-520"]                                                                          kumthim
    Click element                   xpath://*[@id="mobile"]/div/div/div/div[5]/div/div/div[1]/div[1]/div[2]/div/i
    Click element                   xpath://*[@id="list-item-537-1"]/div/div
    Click button                    xpath://*[@id="check-signup-form"]/button
    Sleep                           3s
    Close browser



