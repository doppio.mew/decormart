*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${BROWSER}         chrome
${URL_QA}          https://qa.decormartph.com
${URL_Preprod}     https://preprod.decormartph.com

*** Keywords ***


*** Test Cases ***
Verify "Quick view" popup
    Open BROWSER                    ${URL_QA}  ${BROWSER}
    Maximize Browser Window
    Wait Until Element Contains     xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button[1]/span                  SIGN IN
    Click element                   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/div[1]/div
    Click element                   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/div[2]/div[6]/div/div[1]/a/div
    Sleep                           3s
    Click button                    xpath://*[@id="app"]/div/div[3]/div[2]/div[1]/div/div[2]/div[1]/div/div/div/div[2]/div/button
    Scroll element into view        xpath://*[@id="app"]/div/div[3]/div[2]/div[1]/div/div[3]/button/span
    Click button                    xpath://*[@id="app"]/div/div[3]/div[2]/div[1]/div/div[2]/div[3]/div/div/div[2]/div/button
    Element Should Contain          xpath://*[@id="app"]/div/div[3]/div[2]/div[2]/div/div[2]/div/div[2]/span[2]                                         Kitchen Sink
    Close browser
