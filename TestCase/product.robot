*** Settings ***
Library         SeleniumLibrary
Resource        ../Resources/Resources.robot
Test Setup      Open Browser And Maximize
Test Teardown   Close Browser Window

*** Test Cases ***
Verify "Quick view" popup
    Click element                   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/a/div
    Sleep                           3s
    Mouse over                      xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[1]/a/div
    Sleep                           3s
    Click element                   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[2]/div[3]/div/a/div/strong
    Sleep                           2s
    mouse over                      xpath://*[@id="app"]/div/div[3]/div[2]/div[2]/div/div[2]/div[1]/div[1]/img
    click element                   xpath://*[@id="app"]/div/div[3]/div[2]/div[2]/div/div[2]/div[1]/div[1]/div
    Sleep                           2s
    Element Should Contain          xpath://*[@id="app"]/div/div[2]/div/div/div/div[2]/div/div/div/div[2]/div[3]/div[2]/div/div                         Category : Kitchen Sink
    Sleep                           2s

