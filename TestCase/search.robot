*** Settings ***
Library         SeleniumLibrary
Library         DataDriver  ../TestData/setA.xlsx
Resource        ../Resources/Resources.robot
Test Setup      Open Browser And Maximize
Test Template   Search product
Test Teardown   Close Browser Window


*** Keywords ***
Search product
    [Arguments]     ${kw}   ${Search}
    Input Text                      ${Input_Search}     ${kw}
    Sleep                           3s
    Wait Until Element Is Visible   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[1]/div/div/div/div
    Click button                    xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[1]/div/form/div/div/div/div[2]/button
    Sleep                           3s
    Element Should Contain          xpath://*[@id="app"]/div/div[3]/div[1]/ul/li[3]/div/span/b      ${Search}

*** Test Cases ***
Vaild keywork using ${kw} and ${Search}





#Search product by keyword
#    Input Text                      ${Input_Search}     White
#    Sleep                           3s
#    Wait Until Element Is Visible   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[1]/div/div/div/div
#    Click button                    xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[1]/div/form/div/div/div/div[2]/button
#    Sleep                           3s
#    Element Should Contain          xpath://*[@id="app"]/div/div[3]/div[1]/ul/li[3]/div/span                                                            Search result for "White"

#Search product by SKU
#    Input Text                      ${Input_Search}     10000192
#    Sleep                           3s
#    Wait Until Element Is Visible   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[1]/div/div/div/div
#    Click button                    xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[1]/div/form/div/div/div/div[2]/button
#    Sleep                           3s
#    Element text should be          xpath://*[@id="app"]/div/div[3]/div[1]/ul/li[3]/div/span                                                            Search result for "10000192" (1)
#    Element text should be          xpath://*[@id="app"]/div/div[3]/div[2]/div[2]/div/div[2]/div/div[2]/span[2]                                         Mr. Seller Porcelain Tiles 60x60 PORCELAIN
#
#Search product by Category
#    Click Element                   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/a/div
#    Sleep                           3s
#    Mouse Over                      xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[1]/a/div
#    Click Element                   xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[2]/div[6]/a[2]/div
#    Sleep                           3s
#    Element text should be          xpath://*[@id="app"]/div/div[3]/div[1]/ul/li[7]/div/span                                                                        Paints
#    Element text should be          xpath://*[@id="app"]/div/div[3]/div[2]/div[2]/div/div[1]/span[1]                                                                Showing 1 - 40 of 550 results
#    Element text should be          xpath://*[@id="app"]/div/div[3]/div[2]/div[1]/div/div[1]/span[1]                                                                Paints (550)
