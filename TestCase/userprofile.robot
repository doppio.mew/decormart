*** Settings ***
Library         SeleniumLibrary
Resource        ../Resources/Resources.robot
Test Setup      Open Browser And Maximize
Test Teardown   Close Browser Window

*** Test Cases ***
Update profile name
    Login To mobile account
    Click element                   ${Avatar}
    Sleep                           2s
    Click element                   xpath://*[@id="list-item-497"]/div
    Sleep                           2s
    Click element                   xpath://*[@id="app"]/div/div[3]/div/div[2]/div/div[2]/a/span/i
    Sleep                           2s
    click element                   xpath://*[@id="app"]/div/div[3]/div/div[2]/div/div/div[3]/div[1]/div[2]
    Sleep                           3s
    Input text                      xpath://*[@id="input-892"]                                                                               AAA
    Click button                    xpath://*[@id="app"]/div/div[3]/div/div[2]/div/div/div[6]/div/button
    Wait until element contains     xpath://*[@id="app"]/div/div[1]/div/div[1]                                                               Edit profile successful
    Reload Page
    Sleep                           2s
    Element text should be          xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button/span         QA-SIGNIN02AAA MOBILE

Update billing address
    Login To mobile account
    Click element                   ${Avatar}
    Sleep                           2s
    Click element                   xpath://*[@id="list-item-501"]/div
    Sleep                           2s
    Click element                   xpath://*[@id="app"]/div/div[3]/div/div[2]/div/div/div/div/div/div[1]/div[2]/div/div[2]/div/a[2]
    Sleep                           2s
    Click element                   xpath://*[@id="billing-address"]/div/div/div/div/div/div/div[2]/button[1]/span/span
    Click button                    xpath://*[@id="app"]/div/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div/div/div/div[2]/div[2]/button[2]
    Sleep                           2s
    Input text                      xpath://*[@id="input-997"]                                                                                      99
    Input text                      xpath://*[@id="input-1000"]                                                                                     99
    Click button                    xpath://*[@id="step-billing-address-form"]/div/div/div[12]/button[1]
    Sleep                           2s
    Element text should be          xpath://*[@id="billing-address"]/div/div/div/div/div/div/div[1]/div[1]                                          QAtest99 billing0199