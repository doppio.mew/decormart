*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${BROWSER}          chrome
${URL}              https://preprod.bigthailand.com/
${Login_MESSAGE}    เข้าสู่ระบบด้วย

*** Keywords ***


*** Test Cases ***
TC_UserProfile01
    Open BROWSER                        ${URL}  ${BROWSER}
    Maximize Browser Window
    Wait Until Element Is Visible       xpath://*[@id="__layout"]/div/div/header/div[1]/div[4]/div[2]/div/img[1]
    Click element                       xpath://*[@id="__layout"]/div/div/header/div[1]/div[4]/div[2]/div/img[1]
    Sleep                               3s
    Element Text Should Be              xpath://*[@id="login"]/form/p                                                   ${Login_MESSAGE}
    Sleep                               3s
    Close browser



