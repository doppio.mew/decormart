*** Settings ***
Library         SeleniumLibrary
Resource        ../Resources/Resources.robot
Test Setup      Open Browser And Maximize
Test Teardown   Close Browser Window

*** Test Cases ***
Login via mobile 10 digit
    Login To mobile account
    Wait Until Element Contains     xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button/span     QA-SIGNIN02 MOBILE

Login via email
    Login To Email account
    Wait Until Element Contains     xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button/span     ANO99 ANG99

Login via facebook
    Login To Facebook account
    Wait Until Element Contains     xpath://*[@id="app"]/div/div[2]/div/header[1]/div/div/div/div[2]/div[1]/div[2]/span/button/span     QAMOBILE TEST*//*



